package com.example.tushu.employeeforum.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tushu.employeeforum.R


/**
 * A simple [Fragment] subclass.
 */
class DiscussionFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_discussion,container,false)
    }


}// Required empty public constructor
