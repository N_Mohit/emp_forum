package com.example.tushu.employeeforum.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.example.tushu.employeeforum.R
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(),
    GestureDetector.OnGestureListener {

    var mAuth: FirebaseAuth?=null
    var user: FirebaseUser?=null
    var mAuthListener: FirebaseAuth.AuthStateListener?=null
    var mDatabase: DatabaseReference?=null

    var loginRelativeLayoutDown:Boolean = false
    var signUpRelativeLayoutDown:Boolean = false
    var gDetector: GestureDetectorCompat? = null

    var timer:Timer?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.gDetector = GestureDetectorCompat(this, this)

        loginRelativeLayout.animate()
                .translationY(700F)
                .setDuration(0)
                .start()

        loginRelativeLayoutDown = true

        signUpRelativeLayout.animate()
                .translationY(700F)
                .setDuration(0)
                .start()

        signUpRelativeLayoutDown = true

        homeLoginButton.setOnClickListener {

            loginSignupLayout.visibility = View.GONE
            loginRelativeLayout.animate()
                    .translationY(-80F)
                    .setDuration(250)
                    .start()

            loginRelativeLayoutDown = false
        }

        homeSignUpButton.setOnClickListener {

            loginSignupLayout.visibility = View.GONE
            signUpRelativeLayout.animate()
                    .translationY(-80F)
                    .setDuration(250)
                    .start()

            signUpRelativeLayoutDown = false
        }

        mAuth=FirebaseAuth.getInstance()
        mAuthListener= FirebaseAuth.AuthStateListener {

            firebaseAuth: FirebaseAuth ->

            user = firebaseAuth.currentUser

            if(user!=null){

                startActivity(Intent(this,DashboardActivity::class.java))
                finish()

            }else{

            }
        }

        loginButton.setOnClickListener {

            var email=usernameEdt.text.toString().trim()
            var password=passwordEdt.text.toString().trim()

            if(!TextUtils.isEmpty(email)||!TextUtils.isEmpty(password)){

                loginUser(email,password)
            }else{

                Toast.makeText(this,"Login Failed", Toast.LENGTH_LONG).show()

            }
        }


        signUpButton.setOnClickListener {

            var email =createEmailEdt.text.toString().trim()
            var password =createPasswordEdt.text.toString().trim()
            var displayName =displayNameEdt.text.toString().trim()

            if (!TextUtils.isEmpty(email) ||
                    !TextUtils.isEmpty(password) ||
                    !TextUtils.isEmpty(displayName) ){

                createAccount(email,password,displayName)

            }else
            {
                Toast.makeText(this,"Please fill out the fields",Toast.LENGTH_LONG).show()

            }


        }


    }

    fun createAccount(email: String, password: String, displayName: String) {

        mAuth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    task: Task<AuthResult> ->



                    if(task.isSuccessful){
                        var currentUser = mAuth!!.currentUser
                        var userId= currentUser!!.uid

                        mDatabase = FirebaseDatabase.getInstance().reference
                                .child("Users")
                                .child(userId)

                        var userObject = HashMap<String,String>()
                        userObject.put("display_name",displayName)
                        userObject.put("status","Hello there...")
                        userObject.put("image","default")
                        userObject.put("thumb_image","default")

                        mDatabase!!.setValue(userObject).addOnCompleteListener {

                            task: Task<Void> ->
                            if (task.isSuccessful){

                                var dashboardIntent = Intent(this, DashboardActivity::class.java)
                                dashboardIntent.putExtra("name",displayName)
                                startActivity(dashboardIntent)
                                finish()

                            }else{

                                Toast.makeText(this,"User Not Created",Toast.LENGTH_LONG).show()


                            }

                        }


                    }else{
                        Log.d("Error", task.exception!!.message)


                    }


                }

    }

    private fun loginUser(email: String, password: String) {

        mAuth!!.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener {

                    task: Task<AuthResult> ->

                    if (task.isSuccessful){

                        var dashboardIntent = Intent(this, DashboardActivity::class.java)
                        startActivity(dashboardIntent)
                        finish()


                    }else{

                        Toast.makeText(this,"Login Failed",Toast.LENGTH_LONG).show()

                    }

                }

    }

    override fun onShowPress(p0: MotionEvent?) {

        return
    }

    override fun onSingleTapUp(p0: MotionEvent?): Boolean {

        return true
    }

    override fun onDown(p0: MotionEvent?): Boolean {

        if (!loginRelativeLayoutDown) {
            loginRelativeLayout.animate()
                    .translationY(700F)
                    .setDuration(250)
                    .start()

            loginSignupLayout.visibility = View.VISIBLE

        }

        if (!signUpRelativeLayoutDown) {
            signUpRelativeLayout.animate()
                    .translationY(700F)
                    .setDuration(250)
                    .start()

            loginSignupLayout.visibility = View.VISIBLE

        }

        return true

    }

    override fun onFling(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {

        return true

    }

    override fun onScroll(p0: MotionEvent?, p1: MotionEvent?, p2: Float, p3: Float): Boolean {

        return true
    }

    override fun onLongPress(p0: MotionEvent?) {

        return
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        this.gDetector?.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    override fun onStart() {
        super.onStart()
        mAuth!!.addAuthStateListener(mAuthListener!!)
    }

    override fun onStop() {
        super.onStop()
        mAuth!!.removeAuthStateListener(mAuthListener!!)
    }
}
