package com.example.tushu.employeeforum.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.tushu.employeeforum.fragments.DiscussionFragment
import com.example.tushu.employeeforum.fragments.ChatFragment


/**
 * Created by tushu on 05/01/18.
 */

class SectionPagerAdapter(fm:FragmentManager):FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        when(position){

            0 ->  return DiscussionFragment()

            1 -> return ChatFragment()

        }
        return null!!

    }

    override fun getCount(): Int {

        return 2

    }

    override fun getPageTitle(position: Int): CharSequence? {

        when(position){

            0 -> return "DISCUSSION"
            1 -> return "Me"

        }

        return null!!
    }


}